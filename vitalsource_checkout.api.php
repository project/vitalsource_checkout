<?php

/**
 * @file
 * Hooks and so on.
 */

/**
 * Alter the data we are about to send to Vitalsource.
 */
function hook_vitalsource_checkout_order_item_alter(\Drupal\vitalsource_checkout\VitalsourceOrderItem $order_item, \Drupal\commerce_product\Entity\ProductVariation $product_variation, \Drupal\commerce_order\Entity\Order $order) {
  // Set the licence type to "numdays" and a total of 90 days.
  $order_item->setLicenceType('numdays');
  $order_item->setNumDays(90);
}

/**
 * React on the fact that a link was generated.
 */
function hook_vitalsource_checkout_item_url(\Drupal\commerce_product\Entity\ProductVariationInterface $variation, $activation_data, \Drupal\vitalsource_checkout\VitalsourceAccountData $account_data) {
  \Drupal::logger('mymodule')->info('Generated url @url', [
    '@url' => $activation_data->activation_url,
  ]);
}
