<?php

namespace Drupal\vitalsource_checkout;

/**
 * VitalsourceAccountData class.
 */
class VitalsourceAccountData {

  /**
   * Response object from Vitalsource API.
   *
   * @var object
   */
  private $vitalUser;

  /**
   * Access token.
   *
   * @var string
   */
  private $accessToken;

  /**
   * Getter.
   */
  public function getVitalUser() {
    return $this->vitalUser;
  }

  /**
   * Setter.
   */
  public function setVitalUser($vitalUser) {
    $this->vitalUser = $vitalUser;
  }

  /**
   * Getter.
   */
  public function getAccessToken() {
    return $this->accessToken;
  }

  /**
   * Setter.
   */
  public function setAccessToken($accessToken) {
    $this->accessToken = $accessToken;
  }

}
