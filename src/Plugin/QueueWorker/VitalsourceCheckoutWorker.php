<?php

namespace Drupal\vitalsource_checkout\Plugin\QueueWorker;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\user\UserDataInterface;
use Drupal\vitalsource_api\VitalsourceApiClientFactory;
use Drupal\vitalsource_checkout\VitalsourceAccountData;
use Drupal\vitalsource_checkout\VitalsourceCheckoutItemExtractor;
use Drupal\vitalsource_checkout\VitalsourceCheckoutMailHandler;
use Drupal\vitalsource_checkout\VitalsourceOrderItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Process vitalsource relevant orders.
 *
 * @QueueWorker(
 *   id = "vitalsource_checkout_queue",
 *   title = @Translation("Process vitalsource relevant orders"),
 *   cron = {"time" = 60}
 * )
 */
class VitalsourceCheckoutWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Client factory.
   *
   * @var \Drupal\vitalsource_api\VitalsourceApiClientFactory
   */
  protected $clientFactory;

  /**
   * Extractor.
   *
   * @var \Drupal\vitalsource_checkout\VitalsourceCheckoutItemExtractor
   */
  protected $extractor;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * User data.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * VS Client.
   *
   * @var \Nymediaas\Vitalsource\Client
   */
  protected $vsClient;

  /**
   * VitalsourceCheckoutWorker constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, VitalsourceApiClientFactory $client_factory, VitalsourceCheckoutItemExtractor $extractor, ConfigFactory $config_factory, ModuleHandlerInterface $module_handler, MailManagerInterface $mail_manager, LanguageManagerInterface $language_manager, UserDataInterface $user_data) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->clientFactory = $client_factory;
    $this->extractor = $extractor;
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
    $this->mailManager = $mail_manager;
    $this->languageManager = $language_manager;
    $this->userData = $user_data;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('vitalsource_api.client'),
      $container->get('vitalsource_checkout.item_extractor'),
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('plugin.manager.mail'),
      $container->get('language_manager'),
      $container->get('user.data')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    // Data should be an order id.
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    if (!$order = $this->entityTypeManager->getStorage('commerce_order')->load($data)) {
      return;
    }
    /** @var \Drupal\commerce_log\LogStorageInterface $log_storage */
    $log_storage = $this->entityTypeManager->getStorage('commerce_log');
    $log_storage->generate($order, 'vitalsource_start')->save();
    // Now try to find the products that needs generating.
    $vital_items = $this->extractor->extractItems($order);
    try {
      $client = $this->getClient();
      $account_data = $this->getAccountData($order);
      $access_token = $account_data->getAccessToken();
      if (empty($access_token->credential->{'@attributes'}->{'access-token'})) {
        throw new \Exception('There was no access token in the access token response');
      }
      $token_string = $access_token->credential->{'@attributes'}->{'access-token'};
      $book_urls = [];
      foreach ($vital_items as $order_item) {
        $item = $order_item->getPurchasedEntity();
        // Now see if we can generate this content for the user.
        $sku = $item->getSku();
        $vitalsource_order_item = new VitalsourceOrderItem();
        $vitalsource_order_item->setSku($sku);
        // Allow it to be altered.
        $this->moduleHandler->alter('vitalsource_checkout_order_item', $vitalsource_order_item, $item, $order);
        $code_data = $client->createCode($vitalsource_order_item->getSku(), $vitalsource_order_item->getLicenceType(), $vitalsource_order_item->getNumCodes(), $vitalsource_order_item->getNumDays());
        if (empty($code_data->code)) {
          throw new \Exception('No code in the code data for sku ' . $sku);
        }
        $order_item->get('vitalsource_code')->setValue($code_data->code);
        $order_item->save();
        $redemption_data = $client->redeemCode($code_data->code, $token_string);
        if (isset($redemption_data->{'error-code'})) {
          throw new \Exception('There was an error doing redemption. The error string was: ' . $redemption_data->{'error-text'});
        }
        // Then just create an activation request. Store the URL and send this
        // to the user.
        $vital_user = $account_data->getVitalUser();
        $brand_destination_url = 'https://bookshelf.vitalsource.com';
        $this->moduleHandler->alter('vitalsource_checkout_brand_destination_url', $brand_destination_url);
        $activation_data = $client->createActivationRequest($vital_user->email, $token_string, $brand_destination_url);
        if (empty($activation_data->activation_url)) {
          throw new \Exception('No activation URL found for sku ' . $sku);
        }
        $log_storage->generate($order, 'vitalsource_url_item', [
          'id' => $item->id(),
          'url' => $activation_data->activation_url,
          'code' => $code_data->code,
        ])->save();
        $this->moduleHandler->invokeAll('vitalsource_checkout_item_url', [
          $item,
          $activation_data,
          $account_data,
        ]);
        $order_item->get('vitalsource_link')->setValue($activation_data->activation_url);
        $order_item->save();
        $book_urls[] = [
          'title' => $item->label(),
          'url' => $activation_data->activation_url,
        ];
      }
      // @todo: Use events instead. They are so much betterer.
      $this->moduleHandler->invokeAll('vitalsource_checkout_book_urls_generated', $book_urls);
      $params = [
        'urls' => $book_urls,
        'order_id' => $order->getOrderNumber(),
      ];
      $mail_language = $this->languageManager->getDefaultLanguage()->getId();
      if ($owner = $order->get('uid')) {
        // See if we can load that user.
        if (!empty($owner->entity)) {
          /** @var \Drupal\user\Entity\User $owner_user */
          $owner_user = $owner->entity;
          $mail_language = $owner_user->getPreferredLangcode();
        }
      }
      $log_storage->generate($order, 'vitalsource_success')->save();
      $log_storage->generate($order, 'vitalsource_email_sent', [
        'email' => $order->getEmail(),
      ])->save();
      $this->mailManager->mail('vitalsource_checkout',
        VitalsourceCheckoutMailHandler::BOOK_LINKS_GENERATED,
        $order->getEmail(),
        $mail_language,
        $params
      );
    }
    catch (\Exception $e) {
      $log_storage->generate($order, 'vitalsource_error', [
        'error' => $e->getMessage(),
      ])->save();
      // Make sure it is requeued.
      throw $e;
    }
  }

  /**
   * Get an access token, given an order.
   *
   * Will re-use a saved token on the user, if possible.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order in question.
   *
   * @throws \Exception
   */
  protected function getAccountData(OrderInterface $order) {
    $client = $this->getClient();
    // This is the default suffix.
    $reference_id = 'drupal-order-' . $order->id();
    $access_token = NULL;
    if ($uid = $order->getCustomerId()) {
      $reference_id = 'drupal-user' . $uid;
      $access_token = $this->userData->get('vitalsource_checkout', $uid, 'vitalsource_access_token');
    }
    $reference = sprintf('%s-%s', $this->configFactory->get('system.site')->get('uuid'), $reference_id);
    if (!$access_token) {
      $billing = $order->getBillingProfile();
      $first_name = 'firstname';
      $last_name = 'lastname';
      if ($billing instanceof ProfileInterface) {
        if ($billing->hasField('address') && !$billing->get('address')->isEmpty()) {
          $address = $billing->get('address')->first()->getValue();
          if (!empty($address['given_name'])) {
            $first_name = $address['given_name'];
          }
          if (!empty($address['family_name'])) {
            $last_name = $address['family_name'];
          }
        }
      }
      $vital_user = $client->createUser($reference, $first_name, $last_name);
      if (empty($vital_user->email)) {
        // No vital source user was created.
        throw new \Exception('No email found in the vital source user data received on API request');
      }
    }
    else {
      // We need to retrieve the user somehow.
      $vital_user = $client->verifyUser($reference);
    }
    // Now retrieve an access token.
    $access_token = $client->getUserAccessToken($reference);
    $data = new VitalsourceAccountData();
    $data->setAccessToken($access_token);
    $data->setVitalUser($vital_user);
    if ($uid) {
      $this->userData->set('vitalsource_checkout', $uid, 'vitalsource_access_token', $access_token);
    }
    return $data;
  }

  /**
   * Get a client instance.
   *
   * @return \Nymediaas\Vitalsource\Client
   *   A VS Client.
   */
  protected function getClient() {
    if (!isset($this->vsClient)) {
      $this->vsClient = $this->clientFactory->getClient();
    }
    return $this->vsClient;
  }

}
