<?php

namespace Drupal\vitalsource_checkout;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Config\ConfigFactory;

/**
 * VitalsourceCheckoutItemExtractor service.
 */
class VitalsourceCheckoutItemExtractor {

  /**
   * Our config.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a VitalsourceCheckoutItemExtractor object.
   */
  public function __construct(ConfigFactory $config_factory) {
    $this->config = $config_factory->get('vitalsource_checkout.settings');
  }

  /**
   * Get the items that are of our type.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   An order to extract from.
   *
   * @return \Drupal\commerce_order\Entity\OrderItemInterface[]
   *   An array of order item entities, if any.
   */
  public function extractItems(OrderInterface $order) {
    // Find out which product type is the vitalsource one.
    $type = $this->config->get('product_type');
    // Then see if we can find some of those here.
    $items = $order->getItems();
    $vital_items = [];
    foreach ($items as $item) {
      if ($item->bundle() == $type) {
        $vital_items[] = $item;
      }
    }
    return $vital_items;
  }

}
