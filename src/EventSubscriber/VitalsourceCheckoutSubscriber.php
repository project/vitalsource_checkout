<?php

namespace Drupal\vitalsource_checkout\EventSubscriber;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\vitalsource_checkout\VitalsourceCheckoutItemExtractor;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Vitalsource checkout event subscriber.
 */
class VitalsourceCheckoutSubscriber implements EventSubscriberInterface {

  const QUEUE_NAME = 'vitalsource_checkout_queue';

  /**
   * Queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queue;

  /**
   * Config.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Item extractor.
   *
   * @var \Drupal\vitalsource_checkout\VitalsourceCheckoutItemExtractor
   */
  protected $itemExtractor;

  /**
   * Log storage.
   *
   * @var \Drupal\commerce_log\LogStorageInterface
   */
  protected $logStorage;

  /**
   * Constructs event subscriber.
   */
  public function __construct(QueueFactory $queue, ConfigFactory $config, VitalsourceCheckoutItemExtractor $item_extractor, EntityTypeManagerInterface $entity_type_manager) {
    $this->queue = $queue;
    $this->config = $config->get('vitalsource_checkout.settings');
    $this->itemExtractor = $item_extractor;
    $this->logStorage = $entity_type_manager->getStorage('commerce_log');
  }

  /**
   * React on orders that are placed of the correct type.
   */
  public function queuePurchase(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    $vital_items = $this->itemExtractor->extractItems($order);
    if (!empty($vital_items)) {
      // Queue the order id, so we dont serialize the whole thing.
      $this->logStorage->generate($order, 'vitalsource_queued')->save();
      $this->queue->get(self::QUEUE_NAME)
        ->createItem($order->id());
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'commerce_order.place.post_transition' => ['queuePurchase', -100],
    ];
  }

}
