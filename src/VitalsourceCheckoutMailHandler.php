<?php

namespace Drupal\vitalsource_checkout;

use Drupal\Core\Render\RendererInterface;

/**
 * VitalsourceCheckoutMailHandler service.
 */
class VitalsourceCheckoutMailHandler {

  const BOOK_LINKS_GENERATED = 'book_links_generated';

  /**
   * Renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructor.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * Sends the mails.
   */
  public function handleMail($key, &$message, $params) {
    if ($key == self::BOOK_LINKS_GENERATED) {
      $this->handleBookLinks($key, $message, $params);
    }
  }

  /**
   * Book links mail.
   */
  protected function handleBookLinks($key, &$message, $params) {
    $message['subject'] = t('Activation links for your purchase');
    $el = [
      '#theme' => 'vitalsource_checkout_activation_links_mail',
      '#urls' => $params['urls'],
    ];
    $message['body'][] = $this->renderer->render($el);
  }

}
