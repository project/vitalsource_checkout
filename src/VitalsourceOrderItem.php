<?php

namespace Drupal\vitalsource_checkout;

/**
 * Class VitalsourceOrderItem.
 */
class VitalsourceOrderItem {

  /**
   * Sku.
   *
   * @var string
   */
  private $sku;

  /**
   * Licence type.
   *
   * @var string
   */
  private $licenceType = 'perpetual';

  /**
   * Number of codes.
   *
   * @var int
   */
  private $numCodes = 1;

  /**
   * Number of days.
   *
   * @var int
   */
  private $numDays;

  /**
   * Get sku.
   */
  public function getSku() {
    return $this->sku;
  }

  /**
   * Set sku.
   */
  public function setSku($sku) {
    $this->sku = $sku;
  }

  /**
   * Getter.
   */
  public function getLicenceType() {
    return $this->licenceType;
  }

  /**
   * Setter.
   */
  public function setLicenceType($licenceType) {
    $this->licenceType = $licenceType;
  }

  /**
   * Getter.
   */
  public function getNumCodes() {
    return $this->numCodes;
  }

  /**
   * Setter.
   */
  public function setNumCodes($numCodes) {
    $this->numCodes = $numCodes;
  }

  /**
   * Getter.
   */
  public function getNumDays() {
    return $this->numDays;
  }

  /**
   * Setter.
   */
  public function setNumDays($numDays) {
    $this->numDays = $numDays;
  }

}
