<?php

namespace Drupal\vitalsource_checkout\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Vitalsource checkout settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vitalsource_checkout_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['vitalsource_checkout.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // @todo: Create dropdown.
    $form['product_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order item type machine name to use for vitalsource checkout'),
      '#default_value' => $this->config('vitalsource_checkout.settings')->get('product_type'),
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('vitalsource_checkout.settings')
      ->set('product_type', $form_state->getValue('product_type'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
